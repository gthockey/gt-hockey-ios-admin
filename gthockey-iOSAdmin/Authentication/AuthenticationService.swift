//
//  AuthenticationService.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/4/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthenticationService: NetworkingManager {

    // MARK: Init
    
    static let shared = AuthenticationService()
    
    // MARK: Public Functions

    public func login(with username: String, _ password: String, completion: @escaping (String) -> Void) {
        AF.request("\(env)/auth/token/login/",
                   method: .post,
                   parameters: ["username": username,
                                "password": password])
            .responseJSON { responseData in
            switch responseData.result {
            case .success(let value):
                let jsonResult = JSON(value)
                
                if let authToken = jsonResult["auth_token"].string {
                    UserDefaults().set(authToken, forKey: "authToken")
                    completion(authToken)
                    return
                }

            case .failure(let error):
                print(error)
            }
        }
    }

    public func logout(completion: @escaping (NSError?) -> Void) {
        guard let authToken = UserDefaults().string(forKey: "authToken") else {
//            completion(.lostAuthToken)
            return
        }
        
        AF.request(
            "\(env)/auth/token/logout/",
            method: .post,
            headers: getHeaders(with: authToken)
        ).responseJSON { response in
            switch response.result {
            case .success(_):
                completion(nil)
            case .failure(_):
                completion(NSError(domain: "Error", code: 1, userInfo: nil))
            }
        }
    }
    
    /// Arranges the headers to be used for networking.
    /// - Parameter authToken: a `String` of the authentication token of the current user
    /// - Returns: a `HTTPHeader` to be used in some networking requests
    private func getHeaders(with authToken: String) -> HTTPHeaders {
        let headers = HTTPHeaders([
            HTTPHeader(name: "Authorization", value: "Token \(authToken)"),
            HTTPHeader(name: "Content-Type", value: "application/x-www-form-urlencoded")
        ])
        return headers
    }

}
