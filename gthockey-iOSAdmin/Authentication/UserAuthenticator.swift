//
//  UserAuthenticator.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/4/21.
//

import Combine
import Foundation

class UserAuthenticator: ObservableObject {
    
    // MARK: Published Variables
    
    @Published(key: "isLoggedIn") var isLoggedin = false
    
    // MARK: Public Functions

    func login(with username: String, _ password: String) {
        // login request... on success:
        AuthenticationService.shared.login(with: username, password) { authToken in
            self.isLoggedin = true
            UserDefaults.standard.set(authToken, forKey: "authToken")
            UserDefaults.standard.set(self.isLoggedin, forKey: "isLoggedIn")
        }
    }

    func logout() {
        AuthenticationService.shared.logout() { error in
            guard error != nil else {
                self.isLoggedin = false
                UserDefaults.standard.set("", forKey: "authToken")
                UserDefaults.standard.set(self.isLoggedin, forKey: "isLoggedIn")
                return
            }
            print("Something went wrong")
        }
    }
}

private var cancellables = [String: AnyCancellable]()

extension Published {
    init(wrappedValue defaultValue: Value, key: String) {
        let value = UserDefaults.standard.object(forKey: key) as? Value ?? defaultValue
        self.init(initialValue: value)
        cancellables[key] = projectedValue.sink { val in
            UserDefaults.standard.set(val, forKey: key)
        }
    }
}
