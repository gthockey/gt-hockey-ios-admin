//
//  TicketView.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 10/1/21.
//

import SwiftUI

struct TicketView: View {
    var body: some View {
        Text("Tickets")
    }
}

struct TicketView_Previews: PreviewProvider {
    static var previews: some View {
        TicketView()
    }
}
