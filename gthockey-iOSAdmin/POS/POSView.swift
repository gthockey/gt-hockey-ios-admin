//
//  POSView.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 9/29/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct POSView: View {
    
    // MARK: Observed Objects
    
    @ObservedObject private var cartViewModel = CartViewModel()
    @ObservedObject private var apparelCoordinator = ApparelCoordinator()
    @ObservedObject private var readerDiscoverer = ReaderDiscoverer()
    
    // MARK: State Variables

    @State private var showingSheet = false
    @State private var presentingModal = false
    
    // MARK: Properties
    
    private let layout = [
        GridItem(.flexible(), spacing: 40),
        GridItem(.flexible(), spacing: 40),
        GridItem(.flexible(), spacing: 40)
    ]
    
    // MARK: Body
    
    var body: some View {
//        VStack {
            ScrollView {
                LazyVGrid(columns: layout, spacing: 40) {
                    ForEach(apparelCoordinator.apparel, id: \.self) { apparel in
                        ApparelCell(apparel: apparel)
                            .onTapGesture {
                                apparelCoordinator.selectedApparel = apparel
                                showingSheet.toggle()
                            }
                    }
                }
                .padding(.vertical)
            }
            .onAppear {
                apparelCoordinator.fetchApparel()
            }
            .sheet(isPresented: $showingSheet) {
                ApparelView(apparelCoordinator: apparelCoordinator,
                            cartViewModel: cartViewModel)
            }
            .fullScreenCover(isPresented: $presentingModal) {
                PaymentView(presentedAsModal: self.$presentingModal,
                            cartViewModel: cartViewModel,
                            readerDiscoverer: readerDiscoverer)
            }
            .padding(.horizontal)
            
        .edgesIgnoringSafeArea(.bottom)
        .navigationTitle("Point of Sale")
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                switch readerDiscoverer.terminalConnectionState {
                case .notConnected:
                    Button("Connect to Terminal") {
                        readerDiscoverer.discoverReadersAction()
                    }
                case .attemptingToConnect:
                    Text("Connecting...")
                case .softwareUpdateInProgress(let percent):
                    Text("Updating Software \(percent)")
                case .connected:
                    Text("Terminal Connected")
                }
            }
        }
        
        if !cartViewModel.cart.items.isEmpty {
            VStack(alignment: .leading) {
                HStack {
                    Text("Cart (\(cartViewModel.cart.items.count))")
                        .font(.title)
                    Spacer()
                    Button(action: {
                        cartViewModel.emptyCart()
                    }, label: {
                        HStack {
                            Text("Clear Cart")
                                .foregroundColor(Color.black)
                        }
                        .padding()
                        .background(Color.red)
                        .cornerRadius(4)
                    })
//                    if readerDiscoverer.isConnectedToTerminal {
                        Button(action: {
                            self.presentingModal = true
                        }, label: {
                            HStack {
                                Text("Checkout (\(cartViewModel.totalString))")
                                    .foregroundColor(Color.black)
                            }
                            .padding()
                            .background(Color.green)
                            .cornerRadius(4)
                        })
//                    }
                }
                .padding(.horizontal)
                CartList(cartViewModel: cartViewModel)
            }
        }
    }

}

struct POSView_Previews: PreviewProvider {
    static var previews: some View {
        POSView()
    }
}
