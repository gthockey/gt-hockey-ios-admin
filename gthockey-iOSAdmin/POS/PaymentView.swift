//
//  PaymentView.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/8/21.
//

import SwiftUI
import SDWebImageSwiftUI
import StripeTerminal

struct PaymentView: View {
    
    @Binding var presentedAsModal: Bool
    @State var presentingConfirmView = true
    
    @ObservedObject var cartViewModel: CartViewModel
    @ObservedObject var readerDiscoverer: ReaderDiscoverer
    
    var body: some View {
        VStack {
            Text(cartViewModel.totalString)
                .bold()
                .font(.largeTitle)
                .padding(.vertical)
            Spacer()
            Text(readerDiscoverer.terminalStatus)
                .font(.title3)
            Spacer()
            Button(action: {
                self.presentedAsModal = false
            }, label: {
                Text("Cancel Sale")
                    .foregroundColor(Color.black)
                    .padding()
                    .background(Color.red)
                    .cornerRadius(4)
            })
        }
        .sheet(isPresented: $presentingConfirmView) {
            ConfirmSaleView(saleIsCorrect: self.$presentedAsModal,
                            presentedAsModal: self.$presentingConfirmView,
                            cartViewModel: cartViewModel,
                            readerDiscoverer: readerDiscoverer)
                .interactiveDismissDisabled(true)
        }
    }
}
