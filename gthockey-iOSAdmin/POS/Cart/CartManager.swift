//
//  CartManager.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/11/21.
//

import Foundation

class CartManager: NetworkingManager {

    // MARK: Init

    override init() {}

    // MARK: Public Functions
    
    func setupPayment(for name: String, _ email: String, with cart: [CartItem], completion: @escaping (String?) -> Void) {
        var params = [String : Any]()
        params["name"] = name
        params["email"] = email
        params["in_person_payment"] = true
        
        var cartArray = [[String: Any]]()
        for item in cart {
            var itemDict = [String: Any]()
            itemDict["item_id"] = item.id
            
            itemDict["quantity"] = item.quantity
            
            var restrictedAttributes = [[String : Any]]()
            for attr in item.restrictedAttributes {
                restrictedAttributes.append(["id": attr.id, "value": attr.value])
            }
            itemDict["options"] = restrictedAttributes
            itemDict["custom_options"] = [[String : Any]]()
            
            cartArray.append(itemDict)
        }
        params["items"] = cartArray
        
        var request = URLRequest(url: URL(string: "\(env)/api/stripe/create-payment-intent/")!,
                                 timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        do {
            let data = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            request.httpBody = data
        } catch {
            print("error")
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            guard
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any],
                let paymentIntentClientSecret = json["paymentIntent"] as? String
            else {
                completion(nil)
                return
            }
            
            completion(paymentIntentClientSecret)
        }
        task.resume()
    }
    
    func capturePaymentIntent(with intentID: String, completion: @escaping (Bool) -> Void) {
        var params = [String : Any]()
        params["intent_id"] = intentID
        
        var request = URLRequest(url: URL(string: "\(env)/api/stripe/capture-payment-intent/")!,
                                 timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        do {
            let data = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            request.httpBody = data
        } catch {
            print("error")
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            guard
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
//                let paymentIntentClientSecret = json["hi"] as? String
            else {
                completion(false)
                return
            }
            
            print(json)
            completion(true)
        }
        task.resume()
    }
}
