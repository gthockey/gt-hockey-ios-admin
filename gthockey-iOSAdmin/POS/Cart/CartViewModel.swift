//
//  CartViewModel.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 10/14/21.
//

import SwiftUI
import Combine

class CartViewModel: ObservableObject {
    
    // MARK: Published Variables
    
//    @Published(key: "cart") var theCart: Any = []
    
    // Others
    
    let passthroughSubject = PassthroughSubject<Cart, Error>()
    
    @Published var cart: Cart = {
        if let cart = UserDefaults.standard.codableObject(dataType: Cart.self, key: "cart") {
            return cart
        }
        return Cart(items: [])
    }()
    
//    @Published var total: Double = 0
    @Published(key: "total") var total: Double = 0
    @Published(key: "totalString") var totalString: String = "$0.00"
    
    
    private var cancellables: Set<AnyCancellable> = []
    
    init() {
        passthroughSubject.sink { completion in
            switch completion {
            case .finished:
                print("finished")
            case .failure(let error):
                print(error.localizedDescription)
            }
        } receiveValue: { value in
            print(value)
            self.cart = value
            // Set the total value of the cart
            var total: Double = 0
            self.cart.items.forEach { total += $0.price * Double($0.quantity) }
            self.total = total
            self.totalString = String(format: "$%.02f", total)
            // Update UserDefaults
            UserDefaults.standard.setCodableObject(Cart(items: self.cart.items), forKey: "cart")
        }
        .store(in: &cancellables)
    }
    
    func addToCart(_ item: CartItem) {
        if var retrievedCart = UserDefaults.standard.codableObject(dataType: Cart.self, key: "cart"), !retrievedCart.items.isEmpty {
            retrievedCart.items.append(item)
            self.passthroughSubject.send(retrievedCart)
        } else {
            // This is the first item in the cart
            self.passthroughSubject.send(Cart(items: [item]))
        }
    }
    
    func deleteItem(at index: Int) {
        if var retrievedCart = UserDefaults.standard.codableObject(dataType: Cart.self, key: "cart"), !retrievedCart.items.isEmpty {
            retrievedCart.items.remove(at: index)
            self.passthroughSubject.send(retrievedCart)
        }
    }
    
    func delete(_ item: CartItem) {
        if var retrievedCart = UserDefaults.standard.codableObject(dataType: Cart.self, key: "cart"), !retrievedCart.items.isEmpty {
            for (index, cartItem) in retrievedCart.items.enumerated() {
                if item == cartItem {
                    retrievedCart.items.remove(at: index)
                    self.passthroughSubject.send(retrievedCart)
                    return
                }
            }
//            retrievedCart.items.remove(at: index)
//            self.passthroughSubject.send(retrievedCart)
        }
    }
    
    func addToCount(at index: Int) {
        if var retrievedCart = UserDefaults.standard.codableObject(dataType: Cart.self, key: "cart"), !retrievedCart.items.isEmpty {
            retrievedCart.items[index].quantity += 1
            self.passthroughSubject.send(retrievedCart)
        }
    }
    
    func emptyCart() {
        self.passthroughSubject.send(Cart(items: []))
    }
}

extension UserDefaults {
    func setCodableObject<T: Codable>(_ data: T?, forKey defaultName: String) {
        let encoded = try? JSONEncoder().encode(data)
        set(encoded, forKey: defaultName)
    }
    
    func codableObject<T: Codable>(dataType: T.Type, key: String) -> T? {
        guard let userDefaultData = data(forKey: key) else {
            return nil
        }
        return try? JSONDecoder().decode(T.self, from: userDefaultData)
    }
}
