//
//  Cart.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/7/21.
//

import Foundation

struct Cart: Codable {
    var items: [CartItem]
}
