//
//  CartList.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 1/4/22.
//

import SwiftUI
import SDWebImageSwiftUI

struct CartList: View {
    
    @ObservedObject var cartViewModel: CartViewModel
        
    var body: some View {
        List {
            ForEach(cartViewModel.cart.items, id: \.self) { item in
                HStack {
                    WebImage(url: item.imageURL)
                        .resizable()
                        .indicator(.activity)
                        .transition(.fade)
                        .scaledToFill()
                        .frame(width: 100, height: 100)
                        .clipped()
                        .scaledToFit()
                        .cornerRadius(14)
                        .overlay(
                            RoundedRectangle(cornerRadius: 14)
                                .stroke(Color.black, lineWidth: 1)
                        )
                    VStack(alignment: .leading) {
                        Text(item.name)
                            .bold()
                        Spacer()
                        ForEach(0..<item.restrictedAttributes.count, id: \.self) { index in
                            Text("\(item.restrictedAttributes[index].key): \(item.restrictedAttributes[index].value)")
                        }
                        Text("Quantity: \(item.quantity)")
                        Spacer()
                    }
                    .padding(.leading)
                    Spacer()
                    Text(String(format: "$%.02f", item.price * Double(item.quantity)))
                }
            }
            .onDelete(perform: removeRows)
        }
    }
    
    func removeRows(at offsets: IndexSet) {
        cartViewModel.delete(cartViewModel.cart.items[offsets.first ?? 0])
    }
}

//struct CartList_Previews: PreviewProvider {
//    static var previews: some View {
//        CartList()
//    }
//}
