//
//  CartAttribute.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/7/21.
//

import Foundation

struct CartAttribute: Codable, Equatable, Hashable {
    var id: Int
    var key: String
    var value: String
    
    func asArray() -> [String : Any] {
        return ["id": id, "key": key, "value": value]
    }
}
