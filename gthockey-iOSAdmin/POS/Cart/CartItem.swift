//
//  CartItem.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 10/12/21.
//

import Foundation

struct CartItem: Codable, Equatable, Hashable {
    static func == (lhs: CartItem, rhs: CartItem) -> Bool {
        return lhs.id == rhs.id &&
                lhs.restrictedAttributes == rhs.restrictedAttributes &&
                lhs.quantity == rhs.quantity
    }
    
    // MARK: Properties
    
    var id: Int
    var name: String
    var imageURL: URL
    var price: Double
    var restrictedAttributes: [CartAttribute]
    var quantity: Int
    
    // MARK: Getters
    
    func getPrice() -> Double {
        return price * 100.0
    }

    func getPriceString() -> String {
        return "$" + String(format: "%.2f", price)
    }

}
