//
//  DropdownPicker.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 10/13/21.
//

import SwiftUI

struct DropdownPicker: View {
    
    // MARK: Binding Variables
    
    @Binding var option: ApparelRestrictedOption
    
    // MARK: State Variables
    
    @State var currentlySelectedIndex: Int = 0
        
    // MARK: Properties
        
    private let layout = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible()),
    ]
    
    // MARK: Body
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text(option.displayName + ":")
                    .bold()
                    .font(.title2)
                Text(option.optionsList[currentlySelectedIndex])
                    .font(.title2)
            }
            
            Picker("", selection: $currentlySelectedIndex) {
                ForEach(option.optionsList.indices, id: \.self) { i in
                    Text(String(option.optionsList[i])).tag(i)
                }
            }
            .pickerStyle(.segmented)
            .onAppear {
                option.value = option.optionsList.first
            }
            .onChange(of: currentlySelectedIndex) { index in
                option.value = option.optionsList[index]
            }
        }
    }

}

//struct DropdownPicker_Previews: PreviewProvider {
//    static var previews: some View {
//        DropdownPicker()
//    }
//}
