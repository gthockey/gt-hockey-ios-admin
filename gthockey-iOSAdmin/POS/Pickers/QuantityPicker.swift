//
//  QuantityPicker.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/6/21.
//

import SwiftUI

struct QuantityPicker: View {
    
    // MARK: Binding Variables
    
    @Binding var quantity: Int
    
    // MARK: Body
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("Quantity:")
                    .bold()
                    .font(.title2)
                Text("\(quantity)")
                    .font(.title2)
            }
            
            Picker("", selection: $quantity) {
                ForEach(1...5, id: \.self) { i in
                    Text(String(i)).tag(i)
                }
            }
            .pickerStyle(.segmented)
        }
    }
    
}


//struct QuantityPicker_Previews: PreviewProvider {
//    static var previews: some View {
//        QuantityPicker(quantity: 1)
//    }
//}
