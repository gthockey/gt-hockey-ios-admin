//
//  ApparelRestrictedOption.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 9/29/21.
//

import Foundation

struct ApparelRestrictedOption: Hashable {
    let id: Int
    let displayName: String
    let helpText: String
    let optionsList: [String]
    let correspondingApparelID: Int
    var value: String?
}
