//
//  ApparelCoordinator.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/5/21.
//

import Foundation

class ApparelCoordinator: ObservableObject {
    
    // MARK: Published Variables
    
    @Published var apparel: [Apparel] = []
    @Published var selectedApparel: Apparel = TestModels.testApparel
    
    // MARK: Public Functions
    
    func fetchApparel() {
        ApparelService.shared.getShopItems(completion: { items in
            self.apparel = items
        })
    }
    
}
