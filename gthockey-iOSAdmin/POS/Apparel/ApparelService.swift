//
//  ApparelService.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/7/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApparelService: NetworkingManager {
    
    // MARK: Init
    
    static let shared = ApparelService()
    
    // MARK: Public Functions
    /**
     Retrieves all shop items from the GT Hockey admin page.

     - Parameter completion: The block containing an array of `Apparel` objects to execute after the get call finishes.
     */
    public func getShopItems(completion: @escaping ([Apparel]) -> Void) {
        var apparels: [Apparel] = []

        AF.request("\(env)/api/shop?brick_and_mortar").validate().responseJSON { responseData in
            switch responseData.result {
            case .success(let value):
                let jsonResult = JSON(value)
                var index = 1
                for (_, value) in jsonResult {
                    if let id = value["id"].int {
                        self.getApparel(with: id, completion: { options in
                            apparels.append(self.makeApparelObject(value: value, options: options))
                            
                            if index == jsonResult.count {
                                completion(apparels)
                            }
                            
                            index += 1
                        })
                    }
                }
                

            case .failure(let error):
                print(error)
            }
        }
    }

    /**
       Retrieves a specific merchandise item from the GT Hockey admin page.

       - Parameter id: An `Int` representation for the merchandise's unique ID value
       - Parameter completion: The block containing a tuple of an array of `ApparelRestricteddOption` objects to execute after the get call finishes.
       */
    public func getApparel(with id: Int, completion: @escaping ([ApparelRestrictedOption]) -> Void) {
        var restrictedOptions: [ApparelRestrictedOption]?
        
        AF.request("\(env)/api/shop/\(id)").validate().responseJSON { responseData in
            switch responseData.result {
            case .success(let value):
                let jsonResult = JSON(value)
                restrictedOptions = self.makeApparelRestrictedOption(value: jsonResult["options"])
                completion(restrictedOptions!)

            case .failure(let error):
                print(error)
            }
        }
    }
    
    // MARK: Private Functions

    private func makeApparelObject(value: JSON, options: [ApparelRestrictedOption]) -> Apparel {
        let apparel = Apparel(id: value["id"].int!,
                              name: value["name"].string!,
                              price: value["price"].double!,
                              description: value["description"].string!,
                              imageURL: URL(string: value["image"].string ??
                                            "https://test.gthockey.com/media/players/caleb.jpg")!,
                              restrictedOptions: options)
        return apparel
    }

    private func makeApparelRestrictedOption(value: JSON) -> [ApparelRestrictedOption] {
        var arr: [ApparelRestrictedOption] = []
        for (_, item) in value {
            let apparelRestrictedOption = ApparelRestrictedOption(id: item["id"].int!,
                                                              displayName: item["display_name"].string!,
                                                              helpText: item["help_text"].string!,
                                                              optionsList: (item["option_list"].string!)
                                                                            .components(separatedBy: [","]),
                                                              correspondingApparelID: item["shop_item"].int!)
            arr.append(apparelRestrictedOption)
        }
        return arr
    }

}
