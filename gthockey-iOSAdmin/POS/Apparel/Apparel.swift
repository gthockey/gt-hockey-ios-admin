//
//  Apparel.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 9/29/21.
//

import Foundation

struct Apparel: Hashable {
    let id: Int
    let name: String
    let price: Double
    let description: String
    let imageURL: URL
    var restrictedOptions: [ApparelRestrictedOption]
}
