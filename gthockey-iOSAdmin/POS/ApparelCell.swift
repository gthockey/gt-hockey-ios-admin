//
//  ApparelCell.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/7/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct ApparelCell: View {
    
    // MARK: Properties
    
    var apparel: Apparel
    
    // MARK: Body
    
    var body: some View {
        ZStack(alignment: .topTrailing) {
            VStack(alignment: .leading, spacing: 12) {
                GeometryReader { geo in
                    WebImage(url: apparel.imageURL)
                        .resizable()
                        .indicator(.activity)
                        .transition(.fade)
                        .scaledToFill()
                        .frame(height: geo.size.width)
                }
                .clipped()
                .scaledToFit()
                .cornerRadius(14)
                .overlay(
                    RoundedRectangle(cornerRadius: 14)
                        .stroke(Color.black, lineWidth: 1)
                )
                
                VStack(alignment: .leading, spacing: 8) {
                    Text(apparel.name)
                        .font(.title2)
                        .bold()
                        .lineLimit(1)
                    Text(String(format: "$%.02f", apparel.price))
                }
            }
        }
    }
}

struct ApparelCell_Previews: PreviewProvider {
    static var previews: some View {
        ApparelCell(apparel: TestModels.testApparel)
    }
}
