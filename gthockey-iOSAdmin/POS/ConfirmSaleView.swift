//
//  ConfirmSaleView.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/9/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct ConfirmSaleView: View {
    
    @Binding var saleIsCorrect: Bool
    @Binding var presentedAsModal: Bool
    @ObservedObject var cartViewModel: CartViewModel
    @ObservedObject var readerDiscoverer: ReaderDiscoverer
    @State private var name: String = ""
    @State private var email: String = ""
    
    var body: some View {
        VStack {
            NavigationView {
                CartList(cartViewModel: cartViewModel)
                    .navigationTitle("Does this look right?")
            }
            
            VStack {
                VStack(alignment: .leading) {
                    Text("Your Name")
                        .bold()
                    TextField("Name", text: $name)
                        .padding(.bottom)
                    Text("Your Email (for reciept)")
                        .bold()
                    TextField("Email", text: $email)
                        .keyboardType(.emailAddress)
                    Text("All purchases are completed with Stripe Payments services and you will receive an email receipt shortly after a secure checkout. Please note that all sales are final with no returns.")
                        .font(.footnote)
                        .padding(.top)
                }
                .padding()
                HStack {
                    Button(action: {
                        presentedAsModal = false
                        saleIsCorrect = false
                    }, label: {
                        HStack {
                            Text("Something is incorrect")
                                .foregroundColor(Color.black)
                        }
                        .padding()
                        .background(Color.red)
                        .cornerRadius(4)
                    })
                    
                    Button(action: {
                        print("Confirm")
                        CartManager().setupPayment(for: name, email, with: cartViewModel.cart.items, completion: { paymentIntent in
                            print(paymentIntent)
                            if let paymentIntent = paymentIntent {
                                readerDiscoverer.checkoutButtonAction(clientSecret: paymentIntent, completion: { success in
                                    saleIsCorrect = false
                                    if success {
                                        print("Thanks for your purchase")
                                        cartViewModel.emptyCart()
                                        presentedAsModal = false
                                    } else {
                                        print("Something went wrong")
                                        presentedAsModal = false
                                    }
                                })
                            }
                        })
                        presentedAsModal = false
                        saleIsCorrect = true
                    }, label: {
                        Text("Confirm (\(cartViewModel.totalString))")
                            .foregroundColor(Color.black)
                            .padding()
                            .background(Color.green)
                            .cornerRadius(4)
                    })
                }
                .padding()
            }
        }
    }
}
//
//struct ConfirmSaleView_Previews: PreviewProvider {
//    static var previews: some View {
//        ConfirmSaleView()
//    }
//}
