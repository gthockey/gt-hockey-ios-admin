//
//  ApparelView.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 9/30/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct ApparelView: View {
    
    // MARK: Environment Variables
    
    @Environment(\.presentationMode) var presentationMode
    
    // MARK: Observed Objects
    
    @ObservedObject var apparelCoordinator: ApparelCoordinator
    @ObservedObject var cartViewModel: CartViewModel
    
    // MARK: State Variables
    
    @State private var quantity: Int = 1
                
    // MARK: Body
        
    var body: some View {
        ZStack(alignment: .topTrailing) {
            ScrollView {
                VStack(alignment: .leading) {
                    WebImage(url: apparelCoordinator.selectedApparel.imageURL)
                        .resizable()
                        .scaledToFill()
                        .frame(height: UIScreen.main.bounds.height * 0.3)
                        .clipped()
                    
                    VStack(alignment: .leading, spacing: 12) {
                        Text(apparelCoordinator.selectedApparel.name)
                            .font(.largeTitle)
                            .bold()
                        Text(String(format: "$%.02f", apparelCoordinator.selectedApparel.price))
                            .font(.title2)
                    }
                    .padding()
                    
                    ForEach(0..<apparelCoordinator.selectedApparel.restrictedOptions.count, id: \.self) { index in
                        DropdownPicker(option: $apparelCoordinator.selectedApparel.restrictedOptions[index])
                    }
                    .padding()
                    
                    QuantityPicker(quantity: $quantity)
                        .padding()
                    
                    
                    HStack {
                        Spacer()
                        Button(action: {
                            addToCartButtonTapped()
                            presentationMode.wrappedValue.dismiss()
                        }, label: {
                            HStack {
                                Text("Add to Cart")
                                    .font(.title2)
                                    .foregroundColor(Color.black)
                                Image(systemName: "cart")
                                    .font(.title2)
                                    .foregroundColor(Color.black)
                            }
                            .padding()
                            .background(Color.green)
                            .cornerRadius(4)
                        })
                        Spacer()
                    }
                    .padding()
                }
            }
            
            Button(action: {
                presentationMode.wrappedValue.dismiss()
            }) {
                Image(systemName: "xmark.circle.fill")
            }
            .font(.largeTitle)
            .background(Color.black)
            .clipShape(Circle())
            .foregroundColor(Color.red)
            .padding([.top, .trailing])
        }
        
    }
    
    // MARK: Private Functions
    
    private func addToCartButtonTapped() {
        var restrictedAttrs: [CartAttribute] = []
        
        for restrictedOption in apparelCoordinator.selectedApparel.restrictedOptions {
            if let restrictedOptionValue = restrictedOption.value, restrictedOptionValue != "" {
                let cartAttr = CartAttribute(id: restrictedOption.id,
                                             key: restrictedOption.displayName,
                                             value: restrictedOptionValue)
                restrictedAttrs.append(cartAttr)
            }
        }
        
        let cartItem = CartItem(id: apparelCoordinator.selectedApparel.id,
                                name: apparelCoordinator.selectedApparel.name,
                                imageURL: apparelCoordinator.selectedApparel.imageURL,
                                price: apparelCoordinator.selectedApparel.price,
                                restrictedAttributes: restrictedAttrs,
                                quantity: quantity)
        cartViewModel.addToCart(cartItem)
        
    }
}

//struct ApparelView_Previews: PreviewProvider {
//    static var previews: some View {
//        ApparelView(apparel: TestModels.testApparel,
//                    restrictedOptions: [TestModels.testApparelOption1])
//    }
//}
