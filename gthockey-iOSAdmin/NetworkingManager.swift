//
//  NetworkingManager.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/11/21.
//

import Foundation

class NetworkingManager {
    
    // MARK: Properties
    
    var env: String {
        #if DEBUG
            return "http://127.0.0.1:8000"
        #elseif TEST
            if UserDefaults.standard.string(forKey: "env") == "Test" {
                return "https://test.gthockey.com"
            } else {
                return "https://gthockey.com"
            }
        #else
            if UserDefaults.standard.string(forKey: "env") == "Production" {
                return "https://gthockey.com"
            } else {
                return "https://test.gthockey.com"
            }
        #endif
    }
    
}
