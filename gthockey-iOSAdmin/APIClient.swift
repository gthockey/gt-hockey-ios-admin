//
//  APIClient.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/9/21.
//

import StripeTerminal
import UIKit

// Example API client class for communicating with your backend
class APIClient: NetworkingManager, ConnectionTokenProvider {

    // For simplicity, this example class is a singleton
    static let shared = APIClient()

    // Fetches a ConnectionToken from your backend
    func fetchConnectionToken(_ completion: @escaping ConnectionTokenCompletionBlock) {
        guard let authToken = UserDefaults().string(forKey: "authToken") else {
            completion(nil, nil)
            return
        }
//        completion(
//            "pst_test_YWNjdF8xSlM2VHZDTjhzZVRuc3cxLDJmYnVXTkpaaUtEakcxTndCVnc1WjJmMVZ6bzBGazg_008MpE3YFg",
//            nil
//        )
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        guard let url = URL(string: "\(env)/api/stripe/connection-token/") else {
            fatalError("Invalid backend URL")
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("Token \(authToken)", forHTTPHeaderField: "Authorization")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let task = session.dataTask(with: request) { (data, response, error) in
            if let data = data {
                do {
                    // Warning: casting using `as? [String: String]` looks simpler, but isn't safe:
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                    if let secret = json?["secret"] as? String {
                        completion(secret, nil)
                    }
                    else {
                        let error = NSError(domain: "com.stripe-terminal-ios.example",
                                            code: 2000,
                                            userInfo: [NSLocalizedDescriptionKey: "Missing `secret` in ConnectionToken JSON response"])
                        completion(nil, error)
                    }
                }
                catch {
                    completion(nil, error)
                }
            }
            else {
                let error = NSError(domain: "com.stripe-terminal-ios.example",
                                    code: 1000,
                                    userInfo: [NSLocalizedDescriptionKey: "No data in response from ConnectionToken endpoint"])
                completion(nil, error)
            }
        }
        task.resume()
    }
}
