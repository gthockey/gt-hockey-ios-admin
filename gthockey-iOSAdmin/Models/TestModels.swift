//
//  TestModels.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 9/29/21.
//

import Foundation

struct TestModels {
    
    static let testApparel = Apparel(id: 31,
                                     name: "Jersey",
                                     price: 80.0,
                                     description: "Some description here",
                                     imageURL: URL(string: "https://prod.gthockey.com/media/shop/White3_2.jpg")!, restrictedOptions: [testApparelOption1])
    
    static let testApparelOption1 = ApparelRestrictedOption(id: 30,
                                                          displayName: "Size",
                                                          helpText: "S, M, L",
                                                          optionsList: ["Small", "Medium", "Large"],
                                                          correspondingApparelID: 31)
    
    static let testCartItem = CartItem(id: 31,
                                       name: "Jersey",
                                       imageURL: URL(string: "http://localhost:8000/media/shop/Georgia_Tech_Cursive_2020.jpg")!,
                                       price: 80.0,
                                       restrictedAttributes: [
                                        CartAttribute(id: 1, key: "Size", value: "Medium")
                                       ],
                                       quantity: 1)
    
}
