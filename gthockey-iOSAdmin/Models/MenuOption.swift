//
//  MenuOption.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/4/21.
//

import SwiftUI

/// A single option that lives at the base of the admin app representing one function or task
enum MenuOption: String, CaseIterable, Hashable {
    
    case pointOfSale
    case tickets
    case logout
    
    /// The name to be shown in the menu
    var displayName: String {
        switch self {
        case .pointOfSale: return "Point of Sale"
        case .tickets: return "Tickets"
        case .logout: return "Logout"
        }
    }
    
    /// The `View` that the user will be brought to if the option is selected. If this option
    /// is nil, the user will not be directed to a new `View`.
    var destination: AnyView? {
        switch self {
        case .pointOfSale: return AnyView(POSView())
        case .tickets: return AnyView(TicketView())
        case .logout: return nil
        }
    }
    
}
