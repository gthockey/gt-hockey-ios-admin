//
//  DonationOption.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/19/21.
//

import Foundation

/// A donation option that can live in the checkout of the POS
enum DonationOption: String, CaseIterable, Hashable {
    
    case none
    case five
    case ten
    case fifteen
    case twentyfive
    case custom
    
    /// The name to be shown at checkout
    var displayName: String {
        switch self {
        case .none: return "No thanks"
        case .five: return "5%"
        case .ten: return "10%"
        case .fifteen: return "15%"
        case .twentyfive: return "25%"
        case .custom: return "Custom amount"
        }
    }
    
    /// The `Double` amount associated with the donation option. Value
    /// will be `nil` if the amount is a custom amount.
    var amount: Double? {
        switch self {
        case .none: return 0
        case .five: return 0.05
        case .ten: return 0.1
        case .fifteen: return 0.15
        case .twentyfive: return 0.25
        case .custom: return nil
        }
    }
    
    /// The index value associated with the donation options when listed
    var tag: Int {
        switch self {
        case .none: return 0
        case .five: return 1
        case .ten: return 2
        case .fifteen: return 3
        case .twentyfive: return 4
        case .custom: return 5
        }
    }
    
}
