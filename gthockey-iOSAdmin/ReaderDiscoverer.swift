//
//  ReaderDiscoverer.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 12/11/21.
//

import Foundation
import StripeTerminal

class ReaderDiscoverer: NSObject, ObservableObject, DiscoveryDelegate, BluetoothReaderDelegate {

    @Published var terminalConnectionState: TerminalConnetionState = .notConnected

    enum TerminalConnetionState {
        case notConnected
        case attemptingToConnect
        case softwareUpdateInProgress(Float)
        case connected
    }

//    @Published var isConnectedToTerminal: Bool = false
    @Published var terminalStatus: String = ""
    
    var discoverCancelable: Cancelable?
    var collectCancelable: Cancelable?
    
    var simulated: Bool {
        #if DEBUG
            return true
        #elseif TEST
            return false
        #else
            return false
        #endif
    }
    
    // MARK: BluetoothReaderDelegate
    
    func reader(_ reader: Reader, didReportAvailableUpdate update: ReaderSoftwareUpdate) {
        print("didReportAvailableUpdate")
    }
    
    func reader(_ reader: Reader, didStartInstallingUpdate update: ReaderSoftwareUpdate, cancelable: Cancelable?) {
        print("didStartInstallingUpdate")
    }
    
    func reader(_ reader: Reader, didReportReaderSoftwareUpdateProgress progress: Float) {
        print("didReportReaderSoftwareUpdateProgress")
        print(progress)
        terminalConnectionState = .softwareUpdateInProgress(progress)
    }
    
    func reader(_ reader: Reader, didFinishInstallingUpdate update: ReaderSoftwareUpdate?, error: Error?) {
        print("didFinishInstallingUpdate")
        terminalConnectionState = .notConnected
    }
    
    func reader(_ reader: Reader, didRequestReaderInput inputOptions: ReaderInputOptions = []) {
        print("didRequestReaderInput")
        terminalStatus = "Please insert your card"
    }
    
    func reader(_ reader: Reader, didRequestReaderDisplayMessage displayMessage: ReaderDisplayMessage) {
        print("didRequestReaderDisplayMessage")
        terminalStatus = "Please remove your card"
    }


    // Action for a "Discover Readers" button
    func discoverReadersAction() {
        let config = DiscoveryConfiguration(discoveryMethod: .bluetoothScan, simulated: simulated)
        terminalConnectionState = .attemptingToConnect
        self.discoverCancelable = Terminal.shared.discoverReaders(config, delegate: self) { error in
            if let error = error {
                print("discoverReaders failed: \(error)")
            } else {
                print("discoverReaders succeeded")
            }
        }
    }
    
    func checkoutButtonAction(clientSecret: String, completion: @escaping (Bool) -> Void) {
        // ... Fetch the client secret from your backend
        Terminal.shared.retrievePaymentIntent(clientSecret: clientSecret) { retrieveResult, retrieveError in
            if let error = retrieveError {
                print("retrievePaymentIntent failed: \(error)")
                completion(false)
            } else if let paymentIntent = retrieveResult {
                print("retrievePaymentIntent succeeded: \(paymentIntent)")
                self.collectCancelable = Terminal.shared.collectPaymentMethod(paymentIntent) { collectResult, collectError in//collectPaymentMethod(paymentIntent, delegate: self) { collectResult, collectError in
                    if let error = collectError {
                        print("collectPaymentMethod failed: \(error)")
                        completion(false)
                    }
                    else if let collectPaymentMethodPaymentIntent = collectResult {
                        print("collectPaymentMethod succeeded")
                        // ... Process the payment
                        Terminal.shared.processPayment(collectPaymentMethodPaymentIntent) { processResult, processError in
                            if let error = processError {
                                print("processPayment failed: \(error)")
                                completion(false)
                            } else if let processPaymentPaymentIntent = processResult {
                                print("processPayment succeeded")
                                // Notify your backend to capture the PaymentIntent
                                CartManager().capturePaymentIntent(with: processPaymentPaymentIntent.stripeId) { captured in
                                    if !captured {
                                        print("capture failed")
                                        self.terminalStatus = "Payment Failed"
                                        completion(false)
                                    } else {
                                        print("capture succeeded")
                                        DispatchQueue.main.async {
                                            self.terminalStatus = "Payment Succeeded"
                                        }
                                        completion(true)
                                    }
                                }
                            }
                        }
                    }
                }
                // ...
            }
        }
    }
    
    // ...

    // MARK: DiscoveryDelegate

    // This delegate method can get called multiple times throughout the discovery process.
    // You might want to update a UITableView and display all available readers.
    // Here, we're automatically connecting to the first reader we discover.
    func terminal(_ terminal: Terminal, didUpdateDiscoveredReaders readers: [Reader]) {

        // Select the first reader we discover
        guard let selectedReader = readers.first else { return }

        // Only connect if we aren't currently connected.
        guard terminal.connectionStatus == .notConnected else { return }

        let connectionConfig = BluetoothConnectionConfiguration(
          // When connecting to a physical reader, your integration should specify either the
          // same location as the last connection (selectedReader.locationId) or a new location
          // of your user's choosing.
          //
          // Since the simulated reader is not associated with a real location, we recommend
          // specifying its existing mock location.
            locationId: selectedReader.locationId ?? "tml_EdQgTAuRn77kJV"
        )

        Terminal.shared.connectBluetoothReader(selectedReader, delegate: self, connectionConfig: connectionConfig) { reader, error in
            if let reader = reader {
                print("Successfully connected to reader: \(reader)")
                self.terminalConnectionState = .connected
            } else if let error = error {
                print("connectReader failed: \(error)")
                self.terminalConnectionState = .notConnected
            }
        }
    }
    
}
