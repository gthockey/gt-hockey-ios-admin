//
//  ContentView.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 9/29/21.
//

import SwiftUI

struct ContentView: View {
    
    // MARK: ObservedObjects
    
    @ObservedObject var userAuthenticator = UserAuthenticator()
    @ObservedObject var environmentManager = EnvironmentManager()
    
    // MARK: State Variables
    
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var env: String = ""
    
    // MARK: Body
    
    var body: some View {
        NavigationView {
            if userAuthenticator.isLoggedin {
                List(MenuOption.allCases, id: \.self) { option in
                    if let destination = option.destination {
                        NavigationLink(destination: destination) {
                            Text(option.displayName)
                        }
                    } else {
                        Text(option.displayName)
                            .onTapGesture(perform: {
                                userAuthenticator.logout()
                            })
                    }
                }
                .navigationTitle("Admin Menu")
            } else {
                VStack {
                    Text("Login to use the admin tools")
                        .bold()
                    TextField("Username", text: $username)
                        .autocapitalization(.none)
                    SecureField("Password", text: $password)
                    Button("Login") {
                        userAuthenticator.login(with: username, password)
                    }
                    #if RELEASE
                        Picker("", selection: $env) {
                            ForEach(environmentManager.environments, id: \.self) { env in
                                Text(env).tag(env)
                            }
                        }
                        .pickerStyle(SegmentedPickerStyle())
                        .onChange(of: env) { index in
                            environmentManager.changeEnv(to: env)
                        }
                    #endif
                }
                .padding(.horizontal)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 15.0, *) {
            ContentView()
                .previewInterfaceOrientation(.landscapeLeft)
        } else {
            ContentView()
            // Fallback on earlier versions
        }
    }
}
