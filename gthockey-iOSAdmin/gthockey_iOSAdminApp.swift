//
//  gthockey_iOSAdminApp.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 9/29/21.
//

import SwiftUI
import StripeTerminal

@main
struct gthockey_iOSAdminApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .onAppear {
                    Terminal.setTokenProvider(APIClient.shared)
                }
        }
    }
}
