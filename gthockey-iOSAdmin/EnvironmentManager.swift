//
//  EnvironmentManager.swift
//  gthockey-iOSAdmin
//
//  Created by Caleb Rudnicki on 1/5/22.
//

import Combine
import Foundation

class EnvironmentManager: ObservableObject {
    
    // MARK: Published Variables
    
    @Published(key: "env") var env: String = ""
    
    var environments: [String] = ["Test", "Production"]
    
    // MARK: Public Functions
    
    func changeEnv(to env: String) {
        print("Changing environemt to \(env)")
        self.env = env
    }

    
}
